package ws

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func RunClient() {

	serverPort := commandArgs()

	// 建立连接
	conn, _, err := websocket.DefaultDialer.Dial(fmt.Sprintf("ws://localhost:%d/ws", serverPort), nil)
	if err != nil {
		fmt.Println("连接websocket失败")
		log.Fatal("WebSocket dial error:", err)
	}
	defer conn.Close()
	fmt.Println("连接websocket成功")
	HCI(conn)

}

func HCI(conn *websocket.Conn) {
	// 启动交互界面
	fmt.Println("进入交互界面，按Ctrl+C/exit退出")
	reader := bufio.NewReader(os.Stdin)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	// 无限循环交互
	for {
		select {
		case <-interrupt:
			// 捕获Ctrl+C信号，退出程序
			fmt.Println("\n收到退出信号，程序即将退出")
			os.Exit(0)
		default:
			// 读取用户输入的消息，并进行相应处理
			fmt.Print("请输入消息: ")
			message, _ := reader.ReadString('\n')
			message = strings.TrimSpace(message)

			// 根据用户输入的消息，执行相应逻辑或响应
			if message == "exit" {
				fmt.Println("收到退出指令，程序即将退出")
				os.Exit(0)
			} else {
				// 在这里执行其他逻辑或响应
				err := conn.WriteMessage(websocket.TextMessage, []byte(message))
				if err != nil {
					log.Printf("发送消息失败：%v", err)
				}
				// 等待接收服务器回复的消息
				for {
					_, reply, err := conn.ReadMessage()
					if err != nil {
						log.Fatalf("接收消息失败：%v", err)
					}
					fmt.Println("收到回复消息：", string(reply))
					break
				}
			}
		}
	}
}

func commandArgs() int64 {
	// 定义命令行参数
	var port int64
	flag.Int64Var(&port, "port", 8080, "enter your websocket server port")
	// 解析命令行参数
	flag.Parse()
	return port
}
