package ws

import (
	"log"
	"net/http"
	"testing"
)

func TestRunServer(t *testing.T) {
	http.HandleFunc("/ws", websocketHandler)
	log.Fatal(http.ListenAndServe(":8081", nil))
}
